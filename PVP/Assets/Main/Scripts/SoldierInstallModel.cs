﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SoldierInstallModel
{
    public string Id;

    public GameObject Prefab;
}
