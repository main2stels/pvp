﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class UnitsController : MonoBehaviour
{
    private SoldierBehaviour _currentSoldier;

    [SerializeField]
    private List<SoldierInstallModel> _soldierPrefabs;

    private Dictionary<string, GameObject> _solderDict;

    private List<SoldierBehaviour> _activeSolderOnMap = new List<SoldierBehaviour>();
    private List<SoldierBehaviour> _enemySolderOnMap = new List<SoldierBehaviour>();

    [SerializeField]
    private List<Transform> _startPositions;

    [SerializeField]
    private List<Transform> _enemyStartPositions;

    private ModeType _mode = ModeType.Empty;

    public void SetCurrentSoldier(SoldierBehaviour soldier)
    {
        if (_currentSoldier != null)
            _currentSoldier.ToInactive();

        _currentSoldier = soldier;
        _currentSoldier.Active();
    }

    private void Start()
    {
        _solderDict = _soldierPrefabs.ToDictionary(x => x.Id, x => x.Prefab);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && _mode != ModeType.Empty)
        {
            if (_currentSoldier != null)
            {
                PointerEventData pointerData = new PointerEventData(EventSystem.current);
                List<RaycastResult> resultsData = new List<RaycastResult>();
                pointerData.position = Input.mousePosition;
                EventSystem.current.RaycastAll(pointerData, resultsData);

                if (resultsData.Count > 0)
                {
                    var layer = resultsData[0].gameObject.layer;

                    if (layer != 5)
                    {
                        switch (_mode)
                        {
                            case ModeType.Move:
                                _currentSoldier.DefinePath(resultsData[0].worldPosition);
                                break;
                            case ModeType.Attack:
                                break;
                            case ModeType.Empty:
                                break;
                        }
                        
                    }
                }
            }
        }
    }

    public void Move()
    {
        if (_mode != ModeType.Move)
            return;

        _currentSoldier.Move();
        _currentSoldier.ResetPath();
    }

    public void MoveMode(bool value)
    {
        if (value)
        {
            NextSolder();
            _currentSoldier.ResetPath();
            _mode = ModeType.Move;
        }
        else
        {
            _mode = ModeType.Empty;
        }
    }

    public void AttackMode()
    {
        Mode(ModeType.Attack);
    }

    public void Mode(ModeType mode)
    {
        switch (mode)
        {
            case ModeType.Move:
                _currentSoldier.ResetPath();
                break;
            case ModeType.Attack:
                break;
            case ModeType.Empty:
                break;
        }
        _mode = mode;
    }

    public void NextSolder()
    {
        Debug.Log(_activeSolderOnMap.IndexOf(_currentSoldier));

        if (_currentSoldier == null)
        {
            SetCurrentSoldier(_activeSolderOnMap.FirstOrDefault());
        }
        else
        {
            var index = _activeSolderOnMap.IndexOf(_currentSoldier);
            index = index == _activeSolderOnMap.Count() - 1 ? 0 : index + 1;

            SetCurrentSoldier(_activeSolderOnMap[index]);
        }

        Camera.main.transform.position = new Vector3(_currentSoldier.transform.position.x, Camera.main.transform.position.y, _currentSoldier.transform.position.z - 10);
        Camera.main.transform.LookAt(_currentSoldier.transform);
    }

    public void InstanceSoldier()
    {
        InstanceSoldier(TeamType.My, _startPositions);
        InstanceSoldier(TeamType.Enemy, _enemyStartPositions);
    }

    public void InstanceSoldier(TeamType team, List<Transform> positions)
    {
        for (int i = 0; i < positions.Count; i++)
        {
            var soldier = Instantiate(_solderDict[(i + 1).ToString()]);
            soldier.transform.position = positions[i].position;

            var s = soldier.GetComponent<SoldierBehaviour>();
            s.Init(team);

            if (team == TeamType.My)
                _activeSolderOnMap.Add(s);
            else if (team == TeamType.Enemy)
                _enemySolderOnMap.Add(s);
        }
    }

}