﻿using UnityEngine;
using UnityEngine.EventSystems;

public class TouchController : MonoBehaviour, IPointerClickHandler
{
    public delegate void TouchHandler(PointerEventData eventData);
    public event TouchHandler Touch;

    public void OnPointerClick(PointerEventData eventData)
    {
        Touch(eventData);
    }
}
