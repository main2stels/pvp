﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public delegate void UpdateStep();

    public static event UpdateStep NewStep;

    public void StepUpdate()
    {
        NewStep();
    }
}
