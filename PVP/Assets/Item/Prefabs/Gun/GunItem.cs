﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunItem : Item
{
    [SerializeField]
    private int _currentAmmo;

    [SerializeField]
    private int _maxAmmo;

    [SerializeField]
    private float _maxDistanceAttack;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Reload(int ammo)
    {
        if(ammo > _maxAmmo)
        {
            Debug.LogError("не подходит!!!");
            return;
        }

        SetAmmo(ammo);
    }

    private void SetAmmo(int ammo)
    {
        _currentAmmo = ammo;
    }
}
