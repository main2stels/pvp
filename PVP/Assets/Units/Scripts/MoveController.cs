﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class MoveController : MonoBehaviour
{
    private NavMeshAgent _agent;
    private Animator _animator;
    private NavMeshObstacle _navMeshObstacle;

    private List<GameObject> _pointObjects = new List<GameObject>();

    [SerializeField]
    protected int _energyForStep;

    [SerializeField]
    private GameObject _pointPrefab;

    [SerializeField]
    private GameObject _redPointPrefab;

    private bool _updatePoints = false;
    private int _currentEnergy = 0;

    private Vector3 _oldPath;

    public delegate void PoinDestination();

    public event PoinDestination Destination;

    private bool isDestination = false;

    private bool isActive = false;

    void Start()
    {
        _agent = GetComponent<NavMeshAgent>();
        _animator = GetComponent<Animator>();
        _navMeshObstacle = GetComponent<NavMeshObstacle>();
    }

    void Update()
    {
        MoveInput();
    }

    public virtual void Move()
    {
        if (_agent.isStopped == false)
            return;

        isDestination = true;

        _agent.isStopped = false;
        _animator.SetTrigger("ToRun");

        StartCoroutine(CheckRun());
    }   

    public virtual void DefinePath(Vector3 point, int currentEnergy)
    {
        if (_agent.isStopped == false && _agent.hasPath == true)
            return;

        _agent.SetDestination(point);
        _agent.isStopped = true;
        _updatePoints = true;
        _currentEnergy = currentEnergy;

        ResetPath();
    }

    public virtual void ResetPath()
    {
        foreach (var p in _pointObjects)
            Destroy(p);
    }
    
    public virtual void ToInactive()
    {
        isActive = false;
        if (isDestination)
            return;

        StartCoroutine(SwitchAgent(false));
    }

    public virtual void Active()
    {
        isActive = true;
        StartCoroutine(SwitchAgent(true));
    }

    private IEnumerator SwitchAgent(bool agentStatus)
    {
        _navMeshObstacle.enabled = !agentStatus;

        if(!agentStatus)
            _agent.enabled = agentStatus;

        yield return new WaitForSeconds(0.3f);
        _agent.enabled = agentStatus;
    }

    private void MoveInput()
    {
        if (!_updatePoints)
            return;

        if (!(_agent.path.corners.LastOrDefault() != _oldPath))
            return;

        _pointObjects = new List<GameObject>();
        var backPoint = transform.position;
        var energy = _currentEnergy;

        foreach (var point in _agent.path.corners)
        {
            var nextDistance = Vector3.Distance(backPoint, point);

            if (energy >= nextDistance * _energyForStep)
            {
                backPoint = point;

                var p = Instantiate(_pointPrefab);
                p.transform.position = point;
                _pointObjects.Add(p);

                energy -= (int)nextDistance * _energyForStep;
            }
            else
            {
                if (energy > 0)
                {
                    var r = energy / _energyForStep;
                    var k = (point.z - backPoint.z) / (point.x - backPoint.x);

                    var x = r / Mathf.Sqrt(k * k + 1);
                    if ((point.x - backPoint.x) < 0)
                        x *= -1f;

                    var z = x * k;

                    var dist = Vector3.Distance(point, backPoint);
                    var y = (point.y - backPoint.y) * (float)r / dist;
                    Vector3 lastPosition = new Vector3(x, 0, z) + backPoint;
                    lastPosition.y = y;

                    var p = Instantiate(_pointPrefab);
                    p.transform.position = lastPosition;
                    _pointObjects.Add(p);

                    var pr = Instantiate(_redPointPrefab);
                    pr.transform.position = point;
                    _pointObjects.Add(pr);

                    backPoint = lastPosition;

                    _agent.SetDestination(backPoint);
                    energy = 0;
                }
                else
                {
                    var p = Instantiate(_redPointPrefab);
                    p.transform.position = point;
                    _pointObjects.Add(p);
                }
            }
        }

        _oldPath = _agent.destination;
        _updatePoints = false;

    }

    private IEnumerator CheckRun()
    {
        while (!(Vector3.Distance(_agent.destination, transform.position) < 0.5f))
            yield return new WaitForSeconds(0.1f);
        

        _animator.SetTrigger("ToIdle");

        isDestination = false;

        StartCoroutine(SwitchAgent(isActive));
        Destination();
    }
}
