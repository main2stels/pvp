﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using Zenject;

[RequireComponent(typeof(TouchController))]
[RequireComponent(typeof(AttackController))]
[RequireComponent(typeof(MoveController))]
[RequireComponent(typeof(ItemController))]
public class SoldierBehaviour : MonoBehaviour
{
    public TeamType Team;

    private MoveController _moveController;
    private AttackController _attackController;
    private ItemController _itemController;

    private UnitsController _unitsController;

    private bool _isActive = false;

    [SerializeField]
    protected int _energy;

    protected int _currentEnergy;

    [Inject]
    protected void ZenjectInit(UnitsController unitsController)
    {
        _unitsController = unitsController;
    }

    void Start()
    {
        GetComponent<TouchController>().Touch += Touch;
        _moveController = GetComponent<MoveController>();
        _moveController.Destination += Destination;

        _attackController = GetComponent<AttackController>();
        _itemController = GetComponent<ItemController>();

        GameController.NewStep += UpdateStep;
    }

    public virtual void Init(TeamType team)
    {
        Team = team;
    }

    #region Move
    public virtual void DefinePath(Vector3 point)
        => _moveController.DefinePath(point, _energy);

    public virtual void Move()
      => _moveController.Move();

    public virtual void ResetPath()
        => _moveController.ResetPath();
    #endregion

    public virtual void ToInactive()
    {
        ResetPath();
        _isActive = false;
        _moveController.ToInactive();
    }

    public virtual void Active()
    {
        _isActive = true;
        _moveController.Active();
    }

    public virtual void UpdateStep()
    {
        _currentEnergy = _energy;
    }

    private void Touch(PointerEventData eventData)
    {
        _isActive = true;
        Debug.Log($"Touch:{transform.name}");
        _unitsController.SetCurrentSoldier(this);
    }

    private void Destination()
    {

    }
}
