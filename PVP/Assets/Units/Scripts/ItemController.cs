﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ItemController : MonoBehaviour
{
    [SerializeField]
    private Transform _bipHandWeapon;

    [SerializeField]
    private List<GameObject> _items;

    private GameObject _currentItem;

    // Start is called before the first frame update
    void Start()
    {
        if (_items.Count > 0)
        {
            SetCurrentItem(_items.FirstOrDefault());
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void SetCurrentItem(GameObject item)
    {
        _currentItem = Instantiate(item, _bipHandWeapon);
    }
}
